DROP TABLE public.tokens;
DROP TABLE public.users;
DROP INDEX public.user_id;
DROP INDEX public.user_name;
DROP INDEX public.token_value;