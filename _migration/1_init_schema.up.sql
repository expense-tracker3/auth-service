CREATE TYPE token_type AS ENUM ('ACCESS', 'REFRESH');

CREATE TABLE public.users
(
    id       int GENERATED ALWAYS AS IDENTITY,
    name     text NOT NULL UNIQUE,
    password text NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE public.tokens
(
    id         int GENERATED ALWAYS AS IDENTITY,
    user_id    int                      NOT NULL,
    value      text                     NOT NULL,
    type       token_type               NOT NULL,
    created_at timestamp with time zone NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES users (id)
);

CREATE INDEX user_name ON users
    (
     name
        );

CREATE INDEX user_id ON users
    (
     id
        );

CREATE INDEX token_value ON tokens
    (
     value
        );