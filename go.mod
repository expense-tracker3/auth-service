module auth

go 1.16

require (
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/hiko1129/echo-pprof v1.0.1
	github.com/jackc/pgconn v1.8.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/labstack/echo/v4 v4.2.1
	github.com/lib/pq v1.9.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.10.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/mod v0.4.0 // indirect
	golang.org/x/net v0.0.0-20210331060903-cb1fcc7394e5 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/gorm v1.20.12
	honnef.co/go/tools v0.0.1-2020.1.6 // indirect
)
