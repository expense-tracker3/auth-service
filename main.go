package main

import (
	"auth/app/core"
	"auth/app/metrics"
	"auth/app/repository"
	"auth/app/service"
	"auth/app/transport"
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	minGracePeriod = 3
	maxGracePeriod = 8
)

func initZapLog() *zap.Logger {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	config.EncoderConfig.TimeKey = "timestamp"
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	logger, _ := config.Build()

	return logger
}

func main() {
	loggerMgr := initZapLog()
	zap.ReplaceGlobals(loggerMgr)
	defer loggerMgr.Sync()
	logger := loggerMgr.Sugar()

	ctx, cancelFunc := context.WithCancel(context.Background())
	go shutdown(logger, cancelFunc)

	pr := metrics.New()

	dbCfg, err := repository.GetConfig()
	if err != nil {
		logger.Errorf("can't create config: %+v", err)
		return
	}

	db, err := repository.New(logger, ctx, dbCfg, pr)
	if err != nil {
		logger.Errorf("can't connect to db: %+v", err)
		return
	}

	tr := repository.NewToken(db)
	ur := repository.NewUser(db)

	ts := core.NewTokenService(ctx, tr, ur)

	enc := service.NewEncrypt()

	us := core.NewUserService(ts, ur, enc)

	e := echo.New()

	transport.RegisterEndpoints(e, us, ts, logger, pr)

	go func() {
		log.Println(e.Start(":9775"))
	}()
	<-ctx.Done()
	time.Sleep(minGracePeriod * time.Second)

	ctx, cancel := context.WithTimeout(context.Background(), maxGracePeriod-minGracePeriod)
	defer cancel()

	_ = e.Shutdown(ctx)
}

func shutdown(log *zap.SugaredLogger, cancel context.CancelFunc) {
	signals := make(chan os.Signal, 1)

	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	<-signals
	log.Info("received a termination signal")

	cancel()
}
