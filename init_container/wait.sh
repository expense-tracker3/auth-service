#!/bin/env bash
set -x

for ((i=0; i<=30; i++));
do
  echo $i
	if export PGPASSWORD=$PG_PASSWORD; psql -U $PG_USER -p $PG_PORT -h $PG_HOST -w -c "select 1"; then
		exit 0
	else
		sleep 5
	fi
done

exit 1