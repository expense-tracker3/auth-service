package repository

import (
	"auth/app/errs"
	"auth/app/model"
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	"gorm.io/gorm"

	"github.com/pkg/errors"
)

type Token struct {
	client pgClient
}

func NewToken(cl pgClient) *Token {
	return &Token{client: cl}
}

func (t *Token) Create(ctx context.Context, tok model.Token) error {
	ctx = context.WithValue(ctx, operationKey, "createToken")

	stmt := `
		INSERT INTO tokens (user_id, value, created_at, type) 
		VALUES ($1, $2, $3, $4)
`
	_, err := t.client.Exec(ctx, stmt, tok.UserID, tok.Value, tok.CreatedAt, tok.Type.String())

	return errors.Wrap(err, "can't create token")
}

func (t *Token) Delete(ctx context.Context, tok model.Token) error {
	ctx = context.WithValue(ctx, operationKey, "deleteToken")

	stmt := `
		DELETE FROM tokens  
		WHERE value = $1
`
	_, err := t.client.Exec(ctx, stmt, tok.Value)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return errors.Wrap(errs.NotFoundError{}, "can't delete non-existing token")
		}

		return errors.Wrap(err, "can't delete token")
	}

	return nil
}

func (t *Token) Get(ctx context.Context, value string) (model.Token, error) {
	ctx = context.WithValue(ctx, operationKey, "getTokenByValue")

	var tok model.Token

	stmt := `
		SELECT id, user_id, value, type, created_at
		FROM tokens
		WHERE value = $1
`
	err := t.client.QueryRow(ctx, stmt, value).Scan(&tok.ID, &tok.UserID, &tok.Value, &tok.Type, &tok.CreatedAt)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return model.Token{}, errors.Wrap(errs.NotFoundError{}, "token does not exist")
		}

		return model.Token{}, errors.Wrap(err, "can't fetch token")
	}

	return tok, nil
}

func (t *Token) RemoveOld(ctx context.Context, d time.Duration, kind string) error {
	ctx = context.WithValue(ctx, operationKey, "removeOldTokens")

	var err error

	defer func() {
		if dbErr := recover(); dbErr != nil {
			err = errors.New(fmt.Sprintf("panic during token removal: %v", dbErr))
		}
	}()

	stmt := `
		DELETE FROM tokens  
		WHERE type = $1 AND created_at <= $2
`
	_, err = t.client.Exec(ctx, stmt, kind, time.Now().Add(-d))
	if err != nil {
		return errors.Wrap(err, "can't delete token")
	}

	return err
}
