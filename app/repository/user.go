package repository

import (
	"auth/app/errs"
	"auth/app/model"
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
)

type User struct {
	client  pgClient
}

func NewUser(cl pgClient) *User {
	return &User{client: cl}
}

func (u *User) Create(ctx context.Context, us model.User) error {
	ctx = context.WithValue(ctx, operationKey, "createUser")

	stmt := `
		INSERT INTO users (name, password) 
		VALUES ($1, $2)
`
	_, err := u.client.Exec(ctx, stmt, us.Name, us.Password)

	return errors.Wrap(err, "can't create user")
}

func (u *User) Delete(ctx context.Context, us model.User) error {
	ctx = context.WithValue(ctx, operationKey, "deleteUser")

	stmt := `
		DELETE FROM users
		WHERE id = $1
`
	_, err := u.client.Exec(ctx, stmt, us.ID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return errors.Wrap(errs.NotFoundError{}, "can't delete non-existing user")
		}

		return errors.Wrap(err, "can't delete user")
	}

	return nil
}

func (u *User) Get(ctx context.Context, ID uint) (model.User, error) {
	ctx = context.WithValue(ctx, operationKey, "getUserById")

	var us model.User

	stmt := `
		SELECT id, name, password
		FROM users
		WHERE id = $1
`
	err := u.client.QueryRow(ctx, stmt, ID).Scan(&us.ID, &us.Name, &us.Password)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.User{}, errors.Wrap(errs.NotFoundError{}, "user does not exist")
		}

		return model.User{}, errors.Wrap(err, "can't fetch user")
	}

	return us, nil
}

func (u *User) GetByName(ctx context.Context, name string) (model.User, error) {
	ctx = context.WithValue(ctx, operationKey, "getUserByName")

	var us model.User

	stmt := `
		SELECT id, name, password
		FROM users
		WHERE name = $1
`
	err := u.client.QueryRow(ctx, stmt, name).Scan(&us.ID, &us.Name, &us.Password)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.User{}, errors.Wrap(errs.NotFoundError{}, "user does not exist")
		}

		return model.User{}, errors.Wrap(err, "can't fetch user")
	}

	return us, nil
}
