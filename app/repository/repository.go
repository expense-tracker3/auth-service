package repository

import (
	"auth/app/metrics"
	"context"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/golang-migrate/migrate/v4/source/github"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

const operationKey = "operation"

type pgClient interface {
	Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
}

type (
	Config struct {
		Host          string
		User          string
		Password      string
		DBName        string
		Port          int
		SSLMode       string
		Timezone      string
		MaxConnNumber int
	}
)

type meteredPgClient struct {
	pool    *pgxpool.Pool
	metrics *metrics.PromService
}

func (m *meteredPgClient) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	operation := fmt.Sprintf("%v", ctx.Value(operationKey))

	m.metrics.DbTraffic.WithLabelValues(operation).Add(1)
	now := time.Now()

	res, err := m.pool.Exec(ctx, sql, args...)
	if err != nil {
		m.metrics.DbErrors.WithLabelValues(operation).Add(1)
		m.metrics.DbLatency.WithLabelValues(operation, "fail").Observe(float64(time.Since(now).Milliseconds()))

		return nil, err
	}

	m.metrics.DbLatency.WithLabelValues(operation, "success").Observe(float64(time.Since(now).Milliseconds()))

	return res, nil
}

func (m *meteredPgClient) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	operation := fmt.Sprintf("%v", ctx.Value(operationKey))

	m.metrics.DbTraffic.WithLabelValues(operation).Add(1)

	defer func(t time.Time) {
		m.metrics.DbLatency.WithLabelValues(operation, "success").Observe(float64(time.Since(t).Milliseconds()))
	}(time.Now())

	res := m.pool.QueryRow(ctx, sql, args...)

	return res
}

func New(log *zap.SugaredLogger, ctx context.Context, config *Config, m *metrics.PromService) (pgClient, error) {
	url := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=%v&TimeZone=%v",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.DBName,
		config.SSLMode,
		config.Timezone,
	)

	err := migrateSchema(url)
	if err != nil {
		return nil, errors.Wrap(err, "can't migrate schema")
	}

	if config.MaxConnNumber != 0 {
		url += fmt.Sprintf("&pool_min_conns=2&pool_max_conns=%v", config.MaxConnNumber)
	}

	dbpool, err := pgxpool.Connect(ctx, url)
	if err != nil {
		return nil, errors.Wrap(err, "can't connect to database")
	}

	go func() {
		<-ctx.Done()

		log.Info("closing pg pool")
		dbpool.Close()
	}()

	return &meteredPgClient{
		pool:    dbpool,
		metrics: m,
	}, nil
}

func GetConfig() (*Config, error) {
	portStr := os.Getenv("PG_PORT")
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, errors.Wrap(err, "can't convert port value to int")
	}

	return &Config{
		Host:          os.Getenv("PG_HOST"),
		User:          os.Getenv("PG_USER"),
		Password:      os.Getenv("PG_PASSWORD"),
		DBName:        os.Getenv("PG_DBNAME"),
		Port:          port,
		SSLMode:       "disable",
		Timezone:      "Europe/Moscow",
		MaxConnNumber: 10,
	}, nil

}

func migrateSchema(dsn string) error {
	m, err := migrate.New("file://_migration", dsn)
	if err != nil {
		return errors.Wrap(err, "can't migrate schema")
	}

	defer m.Close()

	err = m.Up()
	if err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			return nil
		}

		return errors.Wrap(err, "can't migrate schema")
	}

	return nil
}
