// high-level entities that do not depend on any implementation detail
package model

import (
	"context"
	"time"
)

type User struct {
	ID       uint   `json:"id,omitempty"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type TokenType int

const (
	ACCESS TokenType = iota
	REFRESH
)

type Token struct {
	ID        uint
	UserID    uint
	Value     string    `json:"value"`
	Type      TokenType `json:"type"`
	CreatedAt time.Time
}

type UserRepository interface {
	Create(context.Context, User) error
	Delete(context.Context, User) error
	Get(context.Context, uint) (User, error)
	GetByName(context.Context, string) (User, error)
}

type TokenRepository interface {
	Create(context.Context, Token) error
	Delete(context.Context, Token) error
	Get(context.Context, string) (Token, error)
	RemoveOld(context.Context, time.Duration, string) error
}
