// use a repository to do stuff with DB
package service

import (
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type Encrypt struct{}

func NewEncrypt() *Encrypt {
	return &Encrypt{}
}

func (e Encrypt) Compare(userP string, storedUserP string) bool {
	return bcrypt.CompareHashAndPassword([]byte(storedUserP), []byte(userP)) == nil
}

func (e Encrypt) Hash(passwd string) (string, error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	if err != nil {
		return "", errors.Wrap(err, "can't hash password")
	}

	return string(hashed), nil
}
