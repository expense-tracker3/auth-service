// operations with users
package core

import (
	"auth/app/errs"
	"auth/app/model"
	"context"

	"github.com/pkg/errors"
)

type UserService interface {
	Create(context.Context, model.User) error
	Login(context.Context, model.User) ([]model.Token, error)
}

type Encrypt interface {
	Compare(string, string) bool
	Hash(string) (string, error)
}

type userService struct {
	userRepo     model.UserRepository
	encrypt      Encrypt
	tokenService TokenService
}

func NewUserService(ts TokenService, ur model.UserRepository, encrypt Encrypt) UserService {
	u := &userService{
		tokenService: ts,
		userRepo:     ur,
		encrypt:      encrypt,
	}

	return u
}

func (s *userService) Create(ctx context.Context, u model.User) error {
	_, err := s.userRepo.GetByName(ctx, u.Name)
	if err == nil {
		return errors.Wrap(errs.AlreadyExists{}, "user with that name already exists")
	}

	hash, err := s.encrypt.Hash(u.Password)
	if err != nil {
		return errors.Wrap(err, "can't hash password")
	}

	u.Password = hash

	return s.userRepo.Create(ctx, u)
}

func (s *userService) Login(ctx context.Context, u model.User) ([]model.Token, error) {
	existing, err := s.userRepo.GetByName(ctx, u.Name)
	if err != nil {
		return nil, err
	}

	res := s.encrypt.Compare(u.Password, existing.Password)

	if !res {
		return nil, errs.BadCredentials{}
	}

	authT, err := s.tokenService.IssueAuth(ctx, existing)
	if err != nil {
		return nil, err
	}

	refreshT, err := s.tokenService.IssueRefresh(ctx, existing)
	if err != nil {
		return nil, err
	}

	return []model.Token{authT, refreshT}, nil
}
