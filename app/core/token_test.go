package core

import (
	"auth/app/errs"
	"auth/app/mocks"
	"auth/app/model"
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestValidateSuccess(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	tokenValue := "dummy"
	trm.On("Get", ctx, tokenValue).Return(model.Token{}, nil).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	tok := model.Token{
		Value: tokenValue,
		Type:  model.ACCESS,
	}

	res, err := ts.Validate(ctx, tok)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.Nil(t, err, "error should be nil")
	assert.Equal(t, true, res, "token should be valid")
}

func TestValidateNotFoundError(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	tokenValue := "dummy"
	trm.On("Get", ctx, tokenValue).Return(model.Token{}, errs.NotFoundError{}).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	tok := model.Token{
		Value: tokenValue,
		Type:  model.ACCESS,
	}

	res, err := ts.Validate(ctx, tok)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.Nil(t, err, "error should be nil")
	assert.Equal(t, false, res, "token should not be valid")
}

func TestValidateError(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	tokenValue := "dummy"
	trm.On("Get", ctx, tokenValue).Return(model.Token{}, errors.New("")).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	tok := model.Token{
		Value: tokenValue,
		Type:  model.ACCESS,
	}

	res, err := ts.Validate(ctx, tok)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.NotNil(t, err, "error should not be nil")
	assert.Equal(t, false, res, "token should not be valid")
}

func TestIssueAuth(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	id := uint(1)
	trm.On("Create", ctx, mock.MatchedBy(func(t model.Token) bool { return t.Type == model.ACCESS && t.UserID == id })).Return(nil).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	u := model.User{
		ID:       id,
		Name:     "username",
		Password: "password",
	}

	res, err := ts.IssueAuth(ctx, u)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.Nil(t, err, "error should be nil")
	assert.Equal(t, model.ACCESS, res.Type, "token type should be ACCESS")
	assert.Equal(t, id, res.UserID, "user ID should match")
}

func TestIssueRefresh(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	id := uint(1)
	trm.On("Create", ctx, mock.MatchedBy(func(t model.Token) bool { return t.Type == model.REFRESH && t.UserID == id })).Return(nil).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	u := model.User{
		ID:       id,
		Name:     "username",
		Password: "password",
	}

	res, err := ts.IssueRefresh(ctx, u)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.Nil(t, err, "error should be nil")
	assert.Equal(t, model.REFRESH, res.Type, "token type should be REFRESH")
	assert.Equal(t, id, res.UserID, "user ID should match")
}

func TestRefresh(t *testing.T) {
	urm := &mocks.UserRepository{}
	trm := &mocks.TokenRepository{}
	ctx := context.Background()

	tokenValue := "dummy"
	userId := uint(1)
	trm.On("Get", ctx, tokenValue).Return(model.Token{UserID: userId}, nil).Once()
	trm.On("Create", ctx, mock.MatchedBy(func(t model.Token) bool { return t.Type == model.ACCESS && t.UserID == userId })).Return(nil).Once()
	urm.On("Get", ctx, userId).Return(model.User{ID: userId}, nil).Once()

	ts := tokenService{
		tokenRepo: trm,
		userRepo:  urm,
	}

	tok := model.Token{
		UserID: userId,
		Value:  tokenValue,
		Type:   model.ACCESS,
	}

	res, err := ts.Refresh(ctx, tok)

	trm.AssertExpectations(t)
	urm.AssertExpectations(t)
	assert.Nil(t, err, "error should be nil")
	assert.Equal(t, model.ACCESS, res.Type, "token type should be REFRESH")
	assert.Equal(t, userId, res.UserID, "user ID should match")
}
