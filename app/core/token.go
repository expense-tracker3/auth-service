// operations with tokens
package core

import (
	"auth/app/errs"
	"auth/app/model"
	"context"
	"crypto/rand"
	"encoding/hex"
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type TokenService interface {
	Validate(context.Context, model.Token) (bool, error)
	IssueAuth(context.Context, model.User) (model.Token, error)
	IssueRefresh(context.Context, model.User) (model.Token, error)
	Refresh(context.Context, model.Token) (model.Token, error)
}

type tokenService struct {
	tokenRepo model.TokenRepository
	userRepo  model.UserRepository
}

func NewTokenService(ctx context.Context, tr model.TokenRepository, ur model.UserRepository) TokenService {
	s := &tokenService{
		tokenRepo: tr,
		userRepo:  ur,
	}
	s.startVacuuming(ctx)

	return s
}

func (s *tokenService) Validate(ctx context.Context, t model.Token) (bool, error) {
	_, err := s.tokenRepo.Get(ctx, t.Value)
	if err != nil {
		if errors.Is(err, errs.NotFoundError{}) {
			return false, nil
		}

		return false, err
	}

	return true, err
}

func (s *tokenService) IssueAuth(ctx context.Context, u model.User) (model.Token, error) {
	tok, err := s.issue(ctx, u, model.ACCESS)
	return tok, errors.Wrap(err, "can't issue auth token")
}

func (s *tokenService) IssueRefresh(ctx context.Context, u model.User) (model.Token, error) {
	tok, err := s.issue(ctx, u, model.REFRESH)
	return tok, errors.Wrap(err, "can't issue refresh token")
}

func (s *tokenService) issue(ctx context.Context, u model.User, kind model.TokenType) (model.Token, error) {
	value := generate()

	t := model.Token{
		Value:     value,
		Type:      kind,
		UserID:    u.ID,
		CreatedAt: time.Now(),
	}

	err := s.tokenRepo.Create(ctx, t)
	if err != nil {
		return model.Token{}, err
	}

	return t, nil
}

func (s *tokenService) Refresh(ctx context.Context, refresh model.Token) (model.Token, error) {
	token, err := s.tokenRepo.Get(ctx, refresh.Value)
	if err != nil {
		return model.Token{}, err
	}

	user, err := s.userRepo.Get(ctx, token.UserID)
	if err != nil {
		return model.Token{}, err
	}

	return s.IssueAuth(ctx, user)
}

func generate() string {
	b := make([]byte, 20)
	if _, err := rand.Read(b); err != nil {
		return ""
	}

	return hex.EncodeToString(b)
}

func (s *tokenService) startVacuuming(ctx context.Context) {
	t := time.NewTicker(time.Second * 20)

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			case <-t.C:
				err := s.tokenRepo.RemoveOld(ctx, time.Second*10, model.ACCESS.String())
				if err != nil {
					zap.S().Errorf("can't remove old access tokens: %v", err)
				}

				err = s.tokenRepo.RemoveOld(ctx, time.Hour*24*10, model.REFRESH.String())
				if err != nil {
					zap.S().Errorf("can't remove old refresh tokens: %v", err)
				}
			}
		}
	}(ctx)
}
