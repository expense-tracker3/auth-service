package transport

import (
	"auth/app/core"
	"auth/app/errs"
	"auth/app/metrics"
	"auth/app/model"
	"github.com/hiko1129/echo-pprof"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"net/http"
)

type (
	HTTP struct {
		e       *echo.Echo
		us      core.UserService
		ts      core.TokenService
		log     *zap.SugaredLogger
		metrics *metrics.PromService
	}
)

func RegisterEndpoints(e *echo.Echo, us core.UserService, ts core.TokenService, log *zap.SugaredLogger, m *metrics.PromService) {
	server := &HTTP{
		e:       e,
		us:      us,
		ts:      ts,
		log:     log,
		metrics: m,
	}

	e.Use(middleware.Recover())

	echopprof.Wrap(e)

	e.PUT("/users", server.CreateUserHandler, metricMiddleware(m))
	e.POST("/users/login", server.GetLoginHandler, metricMiddleware(m))
	e.POST("/tokens/refresh", server.GetRefreshHandler, metricMiddleware(m))
	e.POST("/tokens/validate", server.GetValidateHandler, metricMiddleware(m))

	e.GET("/health", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})
	e.GET("/ready", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})
	e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))

}

func (s *HTTP) CreateUserHandler(c echo.Context) error {
	u := model.User{}
	if err := c.Bind(&u); err != nil {
		return s.fail(c, http.StatusBadRequest, "can't parse user from request", err)
	}

	ctx := c.Request().Context()

	err := s.us.Create(ctx, u)
	if err != nil {
		if errors.Is(err, errs.AlreadyExists{}) {
			return s.fail(c, http.StatusConflict, "user already exists", err)
		}

		return s.fail(c, http.StatusInternalServerError, "can't create user", err)
	}

	err = c.NoContent(http.StatusCreated)
	if err != nil {
		s.log.Errorf("can't send response: %+v\n", err)

		return err
	}

	return nil
}

func (s *HTTP) GetLoginHandler(c echo.Context) error {
	u := model.User{}
	if err := c.Bind(&u); err != nil {
		return s.fail(c, http.StatusBadRequest, "can't parse user from request", err)
	}

	ctx := c.Request().Context()

	tokens, err := s.us.Login(ctx, u)
	if err != nil {
		if errors.Is(err, errs.NotFoundError{}) {
			return s.fail(c, http.StatusNotFound, "user not found", err)
		} else if errors.Is(err, errs.BadCredentials{}) {
			return s.fail(c, http.StatusUnauthorized, "bad credentials", err)
		}

		return s.fail(c, http.StatusInternalServerError, "can't login", err)
	}

	err = c.JSON(http.StatusOK, tokens)
	if err != nil {
		s.log.Errorf("can't send response: %+v\n", err)

		return err
	}

	return nil
}

func (s *HTTP) GetRefreshHandler(c echo.Context) error {
	t := model.Token{}
	if err := c.Bind(&t); err != nil {
		return s.fail(c, http.StatusBadRequest, "can't parse token from request", err)
	}

	ctx := c.Request().Context()

	refresh, err := s.ts.Refresh(ctx, t)
	if err != nil {
		if errors.Is(err, errs.NotFoundError{}) {
			return s.fail(c, http.StatusNotFound, "not found", err)
		}

		return s.fail(c, http.StatusInternalServerError, "can't refresh token", err)
	}

	err = c.JSON(http.StatusOK, refresh)
	if err != nil {
		s.log.Errorf("can't send response: %+v\n", err)

		return err
	}

	return nil
}

func (s *HTTP) GetValidateHandler(c echo.Context) error {
	t := model.Token{}
	if err := c.Bind(&t); err != nil {
		return s.fail(c, http.StatusBadRequest, "can't parse token from request", err)
	}

	ctx := c.Request().Context()

	validate, err := s.ts.Validate(ctx, t)
	if err != nil {
		return s.fail(c, http.StatusInternalServerError, "can't refresh token", nil)
	}

	if !validate {
		return s.fail(c, http.StatusUnauthorized, "invalid token", nil)
	}

	err = c.NoContent(http.StatusOK)
	if err != nil {
		s.log.Errorf("can't send response: %+v\n", err)

		return err
	}

	return nil
}

func (s *HTTP) fail(c echo.Context, status int, msg string, err error) error {
	s.log.Errorf("%v: %+v\n", msg, err)

	err = c.String(status, msg)
	if err != nil {
		s.log.Errorf("can't send response: %+v\n", err)

		return err
	}

	return nil
}
