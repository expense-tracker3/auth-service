package transport

import (
	"auth/app/metrics"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

func metricMiddleware(m *metrics.PromService) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			now := time.Now()
			if err := next(c); err != nil {
				c.Error(err)
				m.NetErrors.WithLabelValues(c.Path(), fmt.Sprintf("%d", c.Response().Status)).Add(1)
			} else if c.Response().Status >= http.StatusBadRequest {
				m.NetErrors.WithLabelValues(c.Path(), fmt.Sprintf("%d", c.Response().Status)).Add(1)
			}

			m.NetTraffic.WithLabelValues(c.Path(), fmt.Sprintf("%d", c.Response().Status)).Add(1)
			passed := time.Since(now)
			m.NetLatency.WithLabelValues(c.Path(), fmt.Sprintf("%d", c.Response().Status)).Observe(float64(passed.Milliseconds()))
			return nil
		}
	}
}
