package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// can we measure timeouts?

// latency - time it takes to service a request
// network latency - histogram by url and response code
// db latency - histogram by method

// traffic - request rate
// network traffic - histogram by url and response code
// db traffic - histogram by method

// errors - count them
// db errors
// non-200 responses - by url and response code

type PromService struct {
	NetLatency *prometheus.HistogramVec
	DbLatency  *prometheus.HistogramVec
	NetTraffic *prometheus.CounterVec
	DbTraffic  *prometheus.CounterVec
	NetErrors  *prometheus.CounterVec
	DbErrors   *prometheus.CounterVec
}

func New() *PromService {

	netLatency := promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "network_latency",
			Help: "time it takes to handle a http request, ms",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
			Buckets: []float64{10, 50, 100, 500},
		},
		[]string{"url", "respCode"},
	)

	dbLatency := promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "db_latency",
			Help: "time it takes to handle a db request, ms",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
			Buckets: []float64{10, 50, 100, 500},
		},
		[]string{"method", "status"},
	)

	netTraffic := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "network_traffic",
			Help: "number of http request",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
		},
		[]string{"url", "respCode"},
	)

	dbTraffic := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "db_traffic",
			Help: "number of db request",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
		},
		[]string{"method"},
	)

	netErrors := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "net_errors",
			Help: "number of network error responses",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
		},
		[]string{"url", "respCode"},
	)

	dbErrors := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "db_errors",
			Help: "number of db error responses",
			ConstLabels: prometheus.Labels{
				"service": "auth-service",
			},
		},
		[]string{"method"},
	)

	return &PromService{
		NetLatency: netLatency,
		DbLatency:  dbLatency,
		NetTraffic: netTraffic,
		DbTraffic:  dbTraffic,
		NetErrors:  netErrors,
		DbErrors:   dbErrors,
	}
}
