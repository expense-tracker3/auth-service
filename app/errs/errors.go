package errs

type (
	NotFoundError  struct{}
	AlreadyExists  struct{}
	BadCredentials struct{}
)

func (b BadCredentials) Error() string {
	return "bad credentials"
}

func (a AlreadyExists) Error() string {
	return "object already exists"
}

func (n NotFoundError) Error() string {
	return "object not found"
}
