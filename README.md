
# Authentication service

## Purpose
This is a naive implementation of an authentication service, which can issue, check and refresh authentication tokens and create users. The main goal is to try to use various libraries and tools.

## Which tools were used?
- echo web server
- golang-migrate to migrate database schema during app startup
- prometheus metrics handler to gather information about 3 out of 4 golden signals (since there's no saturation in the app) for DB and HTTP interactions
- docker to build an image of the app
- kubernetes (minikube) to deploy the application
- helm to deploy the application with a postgresql instance (work in progress)
- pkg/errors to handle errors (work in progress)
- vegeta and pprof to find bottlenecks and find out which load the app can handle (work in progress)
- mockery to generate mocks for tests (work in progress)

## Plans
- add Prometheus to the helm chart
- add grafana to the gelm chart
- write grafana dashboards to monitor the application
- configure zap to do structured logging and add logs to the app
- send logs from containers to a logging system (probably fluentd+graylog)

